<?php

class Conexion{
    public static function conectar(){
        //se utiliza PDO para mayor seguridad
        $link = new PDO("mysql:host=localhost;dbname=cg_bd",
                        "root","");

        $link->exec("set names utf8");

        return $link;
    }
}