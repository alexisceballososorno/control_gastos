<?php
 // utilizar la variable de sesion
 session_start();
?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Nicolapp - web</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="vistas/assets/img/plantilla/favicon.png" rel="icon">
  <link href="vistas/assets/img/plantilla/fapple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="vistas/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="vistas/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="vistas/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="vistas/assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="vistas/assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="vistas/assets/vendor/simple-datatables/style.css" rel="stylesheet">  

  <!-- Template Main CSS File -->
  <link href="vistas/assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.2.2
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body> 
  
  <?php 
  // validar si usuario esta logueado, para redirigir a modulos 
  if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){

   // <!-- ======= Header y sidebar ======= -->
    include "modulos/header.php";
    include "modulos/sidebar.php";

    //<!-- ======= Fin de Header y sidebar ======= --> 

    //<!-- ======= Contenido ======= -->

    // llamar url amigable y cargar modulo
    if(isset($_GET["ruta"])){

      if($_GET["ruta"] == "inicio" || 
         $_GET["ruta"] == "registrar-gastos" || 
         $_GET["ruta"] == "administrar-gastos" || 
         $_GET["ruta"] == "perfil" || 
         $_GET["ruta"] == "FAQ" || 
         $_GET["ruta"] == "soporte" ||
         $_GET["ruta"] == "salir"){

        include "modulos/".$_GET["ruta"].".php";
       //si la ruta no existe redirige a 404
      }else{
        include "modulos/error-404.php";
      }
      // si no hay variable de ruta redirigir a 
    }else{
      include "modulos/inicio.php";
    }
    //<!-- ======= Fin de Contenido ======= -->  
  
    // <!-- ======= Footer ======= -->
    include "modulos/footer.php";

    //<!-- ======= Fin Footer ======= --> 

  }else{ // si no hay inicio de sesion redirige a login
    include "modulos/login.php";
  }

  ?>
  
  <!-- ======= boton top ======= -->
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="vistas/assets/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="vistas/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vistas/assets/vendor/chart.js/chart.min.js"></script>
  <script src="vistas/assets/vendor/echarts/echarts.min.js"></script>
  <script src="vistas/assets/vendor/quill/quill.min.js"></script>
  <script src="vistas/assets/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="vistas/assets/vendor/tinymce/tinymce.min.js"></script>
  <script src="vistas/assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="vistas/assets/js/main.js"></script>

</body>

</html>
