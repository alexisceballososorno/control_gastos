

  <main>
    <div class="container">

      <section class="section error-404 min-vh-100 d-flex flex-column align-items-center justify-content-center">
        <h1>Ooops!</h1>
        <h2>Lo sentimos, la pagina que busca no se encuentra <span><i class="bi bi-emoji-frown"></i></span></h2>
        <p>Ingresa al menú laterar y allí podrás encontrar las páginas disponibles</p>      
        <img src="vistas/assets/img/plantilla/404.png" class="img-fluid" alt="Pagina no encontrada">
        <p>También puedes regresar haciendo <a href="inicio">click aquí</a> </p>  
        

      </section>

    </div>
  </main><!-- End #main -->

  