    <!-- ======= Sidebar ======= -->
    <aside id="sidebar" class="sidebar">

      <ul class="sidebar-nav" id="sidebar-nav">
  
        <li class="nav-item">
          <a class="nav-link collapsed" href="inicio">
            <i class="bi bi-grid"></i>
            <span>Dashboard</span>
          </a>
        </li><!-- End Dashboard Nav -->
  
        <li class="nav-heading">Paginas</li>

        <li class="nav-item">
          <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
            <i class="bi bi-journal-arrow-up"></i><span>Gastos</span><i class="bi bi-chevron-down ms-auto"></i>
          </a>
          <ul id="forms-nav" class="nav-content collapse" data-bs-parent="#sidebar-nav">
            <li>
                <a href="registrar-gastos">
                <i class="bi bi-circle"></i><span>Registrar Gastos</span>
                </a>
            </li>
            <li>
                <a href="administrar-gastos">
                <i class="bi bi-circle"></i><span>Administrar Gastos</span>
                </a>
            </li>
          </ul>
        </li><!-- End Gastos Nav -->
  
        <li class="nav-item">
          <a class="nav-link collapsed" href="perfil">
            <i class="bi bi-person"></i>
            <span>Perfil</span>
          </a>
        </li><!-- End Perfil Nav -->
  
        <li class="nav-item">
          <a class="nav-link collapsed" href="FAQ">
            <i class="bi bi-question-circle"></i>
            <span>Ayuda</span>
          </a>
        </li><!-- End F.A.Q Nav -->
  
      </ul>
  
    </aside><!-- End Sidebar-->