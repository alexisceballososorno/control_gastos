 <!-- ======= Contenido ======= -->

    <main id="main" class="main">

      <div class="pagetitle">
        <h1>Registrar de Gastos</h1>
        <nav>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="inicio">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Gastos</a></li>
            <li class="breadcrumb-item active">Registrar de Gastos</li>
          </ol>
        </nav>
      </div><!-- End Page Title -->
  
      <section class="section">
        <div class="row">            
  
          <div class="col-lg-12">
  
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Registrar Gastos</h5>                
  
                <!-- Custom Styled Validation -->
                <form class="row g-3 needs-validation" novalidate>
                  <div class="col-md-2">
                    <label for="validationCustom01" class="form-label">Fecha</label>
                    <input type="date" class="form-control" id="validationCustom01" required>
                    <div class="valid-feedback">
                        Correcto!
                    </div>
                    <div class="invalid-feedback">
                        fecha invalida.
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label for="validationCustom02" class="form-label">Concepto</label>
                    <input type="text" class="form-control" id="validationCustom02" required>
                    <div class="valid-feedback">
                        Correcto!
                    </div>
                    <div class="invalid-feedback">
                        El concepto no debe estar vacio!
                    </div>
                  </div>
                  <div class="col-md-2">
                    <label for="validationCustom03" class="form-label">Tipo</label>
                    <select class="form-select" id="validationCustom03" required>
                      <option selected disabled value="">Seleccione</option>
                      <option>Ingresos</option>
                      <option>Gastos</option>
                    </select>
                    <div class="valid-feedback">
                        Correcto!
                    </div>
                    <div class="invalid-feedback">
                      Por favor seleccione una opción.
                    </div>
                  </div>                  
                  <div class="col-md-2">
                    <label for="validationCustom04" class="form-label">Tipo de caja</label>
                    <select class="form-select" id="validationCustom04" required>
                      <option selected disabled value="">Seleccione</option>
                      <option>Efectivo</option>
                      <option>Banco</option>
                      <option>Nequi</option>
                      <option>Daviplata</option>
                      <option>Otros</option>
                    </select>
                    <div class="valid-feedback">
                        Correcto!
                    </div>
                    <div class="invalid-feedback">
                      Por favor seleccione una opción.
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label for="validationCustom05" class="form-label">Valor</label>
                    <div class="input-group">
                        <span class="input-group-text">$</span>
                        <input type="numbre" class="form-control" id="validationCustom05" required placeholder="123.456">
                        <div class="valid-feedback">
                            Correcto!
                        </div>
                        <div class="invalid-feedback">
                          Por favor ingrese un valor valido.
                        </div>
                    </div>                    
                  </div>
                  <div class="col-12">
                    <button class="btn" type="submit">Registrar</button>
                  </div>
                </form><!-- End Custom Styled Validation -->
  
              </div>
            </div>      
  
          </div>
        </div>
      </section>
  
    </main><!-- End #main -->  

