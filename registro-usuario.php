<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Nueva Cuenta</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="vistas/assets/img/plantilla/favicon.png" rel="icon">
  <link href="vistas/assets/img/plantilla/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="vistas/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="vistas/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="vistas/assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="vistas/assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="vistas/assets/vendor/simple-datatables/style.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="vistas/assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.2.2
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <main>
    <div class="container">

      <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-10 col-md-6 d-flex flex-column align-items-center justify-content-center">

              <div class="card mb-3">

                <div class="card-body">

                  <div class="pt-4 pb-2">
                    <h5 class="card-title text-center pb-0 fs-4">Crear una cuenta</h5>
                    <p class="text-center small">Ingrese sus datos para crear una nueva cuenta</p>
                  </div>

                  <form class="row g-3 needs-validation" novalidate>
                    <div class="col-6">
                      <label for="yourName" class="form-label">Nombre</label>
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend"><i class="bi bi-person-rolodex"></i></i></span>
                        <input type="text" name="regNombre" class="form-control" id="yourName" required>
                        <div class="invalid-feedback">Por favor, escribe tu nombre!</div>
                      </div>
                    </div>

                    <div class="col-6">
                      <label for="yourEmail" class="form-label">Email</label>
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend"><i class="bi bi-envelope"></i></span>
                        <input type="email" name="regEmail" class="form-control" id="yourEmail" required>
                        <div class="invalid-feedback">Debe ingresar un correo valido!</div>
                      </div>
                    </div>

                    <div class="col-6">
                      <label for="yourUsername" class="form-label">nombre de usuario</label>
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend"><i class="bi bi-person-badge"></i></span>
                        <input type="text" name="regUsuario" class="form-control" id="yourUsername" required>
                        <div class="invalid-feedback">Por favor debe elejir un usuario.</div>
                      </div>
                    </div>

                    <div class="col-6">
                      <label for="yourPassword" class="form-label">Contraseña</label>
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend"><i class="bi bi-key"></i></span>
                        <input type="password" name="regPassword" class="form-control" id="yourPassword" required>
                        <div class="invalid-feedback">Es obligatorio una contraseña!</div>
                      </div>
                    </div>

                    <div class="col-12">
                      <div class="form-check">
                        <input class="form-check-input" name="terms" type="checkbox" value="" id="acceptTerms" required>
                        <label class="form-check-label" for="acceptTerms">Estoy de acuerdo y acepto <a href="#">terminos y condiciones</a></label>
                        <div class="invalid-feedback">Debe aceptar antes de crear la cuenta.</div>
                      </div>
                    </div>
                    <div class="col-12">
                      <button class="btn w-100" type="submit">Crear cuenta</button>
                    </div>
                    <div class="col-12">
                      <p class="small mb-0">si ya tienes cuenta <a href="login">Inicia sesión</a></p>
                    </div>
                  </form>

                </div>
              </div>

            </div>
          </div>
        </div>

      </section>

    </div>
  </main><!-- End #main -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="vistas/assets/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="vistas/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vistas/assets/vendor/chart.js/chart.min.js"></script>
  <script src="vistas/assets/vendor/echarts/echarts.min.js"></script>
  <script src="vistas/assets/vendor/quill/quill.min.js"></script>
  <script src="vistas/assets/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="vistas/assets/vendor/tinymce/tinymce.min.js"></script>

  <!-- Template Main JS File -->
  <script src="vistas/assets/js/main.js"></script>

</body>

</html>