# control de gastos

Proyecto de practica, donde se desarrollara el front y back de una aplicación web para control de gastos personales.

## Requerimientos funcionales 

    - login
    - dashboard
    - perfil de usuario

## Ubicacion de aplicación 

- se genera repositorio en gitlab, para la gestion y control de versiones de la aplicación

```
git remote add origin https://gitlab.com/lloaizaa/control_gastos.git

```

## Equipo de colobaracion

    - Luis Fernando Loaiza Acevedo
    - Alexis Ceballos
    - Daniel Atencia

## Pruebas e implementacion

La aplicacion debera cumplir con su objetivo de llevar el control de los gastos poersonales, siendo esta capaz de sumar las entradas y restar las salidas de dinero

***

## Lenguaje de progración

Se implementara para el front end lenguaje HTML, CSS, Javascript, para esto se utilizara frameworks bootstrap en version 5.

El back end se gestionara bajo lenguaje PHP verisón 8.1.6
gestor de base de datos MariaDB versión 10.4.24

