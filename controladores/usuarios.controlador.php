<?php

class ControladorUsuarios{
    //ingreso de usuario

    public function ctrIngresoUsuario(){
        //validar si recibe datos de formulario
        if(isset($_POST["ingUsuario"])){
            //solo permitir letras y numeros en user y pass
            if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingUsuario"]) && 
                preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingPassword"])){

                    $tabla = "usuarios";

                    $item = "usuario";
                    $valor = $_POST["ingUsuario"];

                    $respuesta = ModeloUsuarios::MdlMostrarUsuarios($tabla, $item, $valor);

                    if($respuesta["usuario"] == $_POST["ingUsuario"] && 
                        $respuesta["password"] == $_POST["ingPassword"]){

                            $_SESSION["iniciarSesion"] = "ok";

                            echo '<script> window.location = "inicio"; </script>';

                        }else{
                            echo '<div class="alert alert-danger">Error al ingresar, vuelve a intentarlo</div>';
                        }

            }

        }


    }
}