<?php
// llamar controladores 
require_once "controladores/plantilla.controlador.php";
require_once "controladores/gastos.controlador.php";
require_once "controladores/usuarios.controlador.php";
require_once "controladores/faq.controlador.php";
// llamar modelos
require_once "modelos/gastos.modelo.php";
require_once "modelos/usuarios.modelo.php";
require_once "modelos/faq.modelo.php";

$plantilla = new ControladorPlantilla();
 //ejecutar metodo para traer vista de plantilla
$plantilla -> ctrPlantilla();